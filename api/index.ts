import { kv } from '@vercel/kv';

export const config = {
  runtime: 'edge'
};

const handler = async (request: Request): Promise<Response> => {
  const reqHost = request.headers.get('host').split(':')[0].split('.').join('_'); //Remove the port from the host header, replace dots with underscores
  const targetUrl: string = await kv.get(reqHost) || await kv.get('default');

  const response = await fetch(targetUrl);
  return response;

};

export default handler;
